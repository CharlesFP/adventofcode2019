use std::cmp::Eq;
use std::cmp::Ord;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::fs::read_to_string;
use std::hash::Hash;
use std::hash::Hasher;
use std::io;
use std::io::BufReader;
use std::io::prelude::*;
use std::io::Result;
use std::iter::Extend;
use std::num;
use std::path::Path;

fn main() -> Result<()> {
    solve_day_6()
}

fn buffer_file(name: String) -> Result<BufReader<File>> {
    let file = Path::new(&name);
    let file = File::open(&file);
    Ok(BufReader::new(file?))
}

//
//Day 6 Functions
//

fn solve_day_6() -> Result<()> {
    let file = buffer_file(String::from("data/day6.txt"))?;
    //let file = buffer_file(String::from("data/scratch"))?;
    let mut orbits: HashMap<String, Vec<String>> = HashMap::new();
    for line in file.lines() {
        let line = line?;
        let mut bodies = line.split(')');

        let big = String::from(bodies.next().unwrap());
        let small = String::from(bodies.next().unwrap());

        let mut cur_big: Vec<String> = orbits.get(&big).unwrap_or(&Vec::new()).clone();
        cur_big.push(small);

        orbits.insert(String::from(big), cur_big);
    };

    let total: u64 = sum_tree(&orbits).values().sum();

    println!("{} total orbits", total);

    let dist: u64 = dist(&orbits);

    println!("{} total transfers", dist);

    Ok(())
}

fn sum_tree(t: &HashMap<String, Vec<String>>) -> HashMap<String, u64> {
    let mut o = HashMap::new();

    for body in t.keys() {
        let mut to_update: Vec<String> = t.get(body).unwrap_or(&Vec::new()).clone();

        while !to_update.is_empty() {
            let i = to_update.pop().unwrap();
            let v = o.get(&i).unwrap_or(&0) + 1;
            o.insert(i.clone(), v);
            let mut new_things = t.get(&i).unwrap_or(&Vec::new()).clone();
            to_update.append(&mut new_things);
        };
    };

    o
}

fn dist(t: &HashMap<String, Vec<String>>) -> u64 {
    let mut i = 0;
    let mut path_to_you = dfs(&t, String::from("COM"), &String::from("YOU")).unwrap();
    path_to_you.reverse();
    let mut path_to_san = dfs(&t, String::from("COM"), &String::from("SAN")).unwrap();
    path_to_san.reverse();
    
    while path_to_you.get(i).unwrap().eq(path_to_san.get(i).unwrap()) { i += 1 };

    (path_to_san.len() + path_to_you.len() - (i * 2 + 2)) as u64
}

fn dfs(t: &HashMap<String, Vec<String>>, src: String, dst: &String) -> Option<Vec<String>> {
    if src == *dst {
        return Some(Vec::new())
    }
    let bodies = t.get(&src).unwrap_or(&Vec::new()).clone();
    for b in bodies {
        match dfs(t, b, dst) {
            Some(mut v) => {
                v.push(src);
                return Some(v)
            },
            None => {},
        };
    };
    
    None
}

//
//Day 5 code
//

fn solve_day_5() -> Result<()> {
    let file = read_to_string("data/day5.txt").expect("Couldn't read file");
    let mut memory: HashMap<i64, i64> = HashMap::new();
    let program: Vec<&str> = file.split(',').collect();

    for (i, s) in program.iter().enumerate() {
        let val: i64 = match s.parse() { Ok(val) => val, _ => 0 };
        memory.insert(i as i64, val);
    };

    run_intcode(memory);

    Ok(())
}

//
//Day 4 code
//

fn solve_day_4() -> Result<()> {
    let mut count = 0;

    for x in 357253..892942 {
        let mut sorted = true;
        let mut dup = false;
        let mut super_dup = false;
        let mut just_matched = false;
        let mut prev = ' ';
        for c in x.to_string().chars() {
            if c == prev {
                if just_matched {
                    dup = false;
                } else {
                    dup = true;
                }
                just_matched = true;
            } else if just_matched && dup {
                super_dup = true;
            } else {
                just_matched = false;
            }
            if c < prev {
                sorted = false;
            }
            prev = c;
        }
        if sorted && (dup || super_dup) {
            count += 1;
        }
    };

    println!("{}", count);

    Ok(())
}

//
//DAY 3 Code
//
#[derive(Hash)]
struct Position {
    x: i64,
    y: i64,
}

impl Ord for Position {
    fn cmp(&self, other: &Self) -> Ordering {
        let me: i64 = self.x.abs() + self.y.abs();
        let them: i64 = other.x.abs() + other.y.abs();
        me.cmp(&them)
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl PartialEq for Position {
    fn eq(&self, other: &Self) -> bool {
        (self.x == other.x) && (self.y == other.y)
    }
}

impl Eq for Position { }

fn solve_day_3_two() -> Result<()> {
    let mut file = buffer_file(String::from("data/day3.txt"))?;
    let mut wire1 = String::new();
    let mut wire2 = String::new();
    file.read_line(&mut wire1)?;
    file.read_line(&mut wire2)?;

    let trace1 = trace_wire_with_steps(wire1)?;
    let trace2 = trace_wire_with_steps(wire2)?;

    let set1: HashSet<&Position> = trace1.keys().collect();
    let set2: HashSet<&Position> = trace2.keys().collect();

    let crossing_points = set1.intersection(&set2);

    let closest = match crossing_points.min_by(|x, y| (trace1.get(x).unwrap() + trace2.get(x).unwrap()).cmp(&(trace1.get(y).unwrap() + trace2.get(y).unwrap()))) { Some(p) => p, _ => return Ok(()) };

    println!("lowest effort crossing point is steps1: {}, steps2: {}", trace1.get(closest).unwrap(), trace2.get(closest).unwrap());

    Ok(())
}

fn trace_wire_with_steps(wire: String) -> Result<HashMap<Position, u64>> {
    let mut path = HashMap::new();

    let mut pos = Position { x: 0, y: 0 };
    let mut steps = 0;

    for cmd in wire.split(',') {
        let mut cmd = String::from(cmd);
        let dir: u64 = match cmd.drain(..1).next() {
            Some('U') => 1,
            Some('D') => 3,
            Some('R') => 2,
            Some('L') => 4,
            _ => 999,
        };

        let dist: i64 = match cmd.parse() { Ok(d) => d, _ => 0 };
        for i in 1..(dist+1) {
            let p = match dir {
                1 => Position { x: pos.x, y: pos.y + i },
                3 => Position { x: pos.x, y: pos.y - i },
                2 => Position { x: pos.x + i, y: pos.y },
                4 => Position { x: pos.x - i, y: pos.y },
                _ => Position { x: 0, y: 0 },
            };
            path.insert(p, (steps + i) as u64 );
        };

        steps += dist;
        pos = match dir {
            1 => Position { x: pos.x, y: pos.y + dist },
            3 => Position { x: pos.x, y: pos.y - dist },
            2 => Position { x: pos.x + dist, y: pos.y },
            4 => Position { x: pos.x - dist, y: pos.y },
            _ => Position { x: 1000, y: 1000 },
        };
    };

    Ok(path)
}

fn solve_day_3() -> Result<()> {
    let mut file = buffer_file(String::from("data/day3.txt"))?;
    let mut wire1 = String::new();
    let mut wire2 = String::new();
    file.read_line(&mut wire1)?;
    file.read_line(&mut wire2)?;

    let trace1 = trace_wire(wire1)?;
    let trace2 = trace_wire(wire2)?;

    let crossing_points = trace1.intersection(&trace2);

    let closest = match crossing_points.min() { Some(p) => p, _ => return Ok(()) };

    println!("closest crossing point is x: {}, y: {}", closest.x, closest.y);

    Ok(())
}

fn trace_wire(wire: String) -> Result<HashSet<Position>> {
    let mut path = HashSet::new();

    let mut pos = Position { x: 0, y: 0 };

    for cmd in wire.split(',') {
        let mut cmd = String::from(cmd);
        let dir: u64 = match cmd.drain(..1).next() {
            Some('U') => 1,
            Some('D') => 3,
            Some('R') => 2,
            Some('L') => 4,
            _ => 999,
        };

        let dist: i64 = match cmd.parse() { Ok(d) => d, _ => 0 };
        for i in 1..(dist+1) {
            let p = match dir {
                1 => Position { x: pos.x, y: pos.y + i },
                3 => Position { x: pos.x, y: pos.y - i },
                2 => Position { x: pos.x + i, y: pos.y },
                4 => Position { x: pos.x - i, y: pos.y },
                _ => Position { x: 0, y: 0 },
            };
            path.insert(p);
        };

        pos = match dir {
            1 => Position { x: pos.x, y: pos.y + dist },
            3 => Position { x: pos.x, y: pos.y - dist },
            2 => Position { x: pos.x + dist, y: pos.y },
            4 => Position { x: pos.x - dist, y: pos.y },
            _ => Position { x: 1000, y: 1000 },
        };
    };

    Ok(path)
}

//
//DAY 2 Functions
//

fn solve_day_2() -> Result<()> {
    let file = read_to_string("data/day2.txt").expect("Couldn't read file");
    let mut memory: HashMap<i64, i64> = HashMap::new();
    let program: Vec<&str> = file.split(',').collect();

    for (i, s) in program.iter().enumerate() {
        let val: i64 = match s.parse() { Ok(val) => val, _ => 0 };
        memory.insert(i as i64, val);
    };

    for noun in 0..99 {
        for verb in 0..99 {
            memory.insert(1, noun);
            memory.insert(2, verb);
            let r = run_intcode(memory.clone());
            if r == 19690720 {
                println!("noun is {} and verb is {}", noun, verb);
                break;
            }
        };
    };

    Ok(())
}

fn run_intcode(mut memory: HashMap<i64, i64>) -> i64 {
    let mut address: i64 = 0;
    loop {
        let op = memory.get(&address).unwrap();
        let opcode = op % 100;
        if opcode == 99 { break }

        println!("{} {}", op, opcode);
        match opcode {
            1 => {
                let s = get_param(&memory, address, 1, (op / 100) % 10);
                let r = get_param(&memory, address, 2, (op / 1000) % 10);
                let dst = get_param(&memory, address, 3, 1);
                memory.insert(dst, s + r);
                address += 4;
            },
            2 => { 
                let s = get_param(&memory, address, 1, (op / 100) % 10);
                let r = get_param(&memory, address, 2, (op / 1000) % 10);
                let dst = get_param(&memory, address, 3, 1);
                memory.insert(dst, s * r);
                address += 4;
            }
            3 => {
                let dst = get_param(&memory, address, 1, 1);
                let mut input = String::new();
                println!("should wait for input here");
                io::stdin().lock().read_line(&mut input);
                let input: i64 = input.trim().parse().unwrap();
                memory.insert(dst, input);
                address += 2;
            }
            4 => {
                let src = get_param(&memory, address, 1, 1);
                println!("{}", memory.get(&src).unwrap());
                address += 2;
            },
            5 => {
                let t = get_param(&memory, address, 1, (op / 100) % 10);
                let dst = get_param(&memory, address, 2, (op / 1000) % 10);
                address = if (t != 0) { dst } else { address + 3 };
            },
            6 => {
                let t = get_param(&memory, address, 1, (op / 100) % 10);
                let dst = get_param(&memory, address, 2, (op / 1000) % 10);
                address = if (t == 0) { dst } else { address + 3 };
            },
            7 => {
                let s = get_param(&memory, address, 1, (op / 100) % 10);
                let r = get_param(&memory, address, 2, (op / 1000) % 10);
                let dst = get_param(&memory, address, 3, 1);
                memory.insert(dst, if (s < r) { 1 } else { 0 });
                address += 4;
            },
            8 => {
                let s = get_param(&memory, address, 1, (op / 100) % 10);
                let r = get_param(&memory, address, 2, (op / 1000) % 10);
                let dst = get_param(&memory, address, 3, 1);
                memory.insert(dst, if (s == r) { 1 } else { 0 });
                address += 4;
            },
            _ => {
                println!("broken");
                break;
            },
        };
    };

    *(memory.get(&0).unwrap())
}

fn get_param(memory: &HashMap<i64, i64>, address: i64, position: i64, mode: i64) -> i64 {
    let x = memory.get(&(address + position)).unwrap();
    match mode {
        0 => *memory.get(x).unwrap(),
        1 => *x,
        _ => 99999999,
    }
}

//
//DAY 1 FUNCTIONS
//

fn solve_day_1() -> Result<()> {
    let file = buffer_file(String::from("data/day1.txt"))?;
    let total_fuel: u32 = file.lines().map(|line|
            match line {
                Ok(line) => match line.parse() {
                    Ok(mass) => calc_fuel_req_tail_rec(mass, 0),
                    _ => 0,
                },
                _ => 0,
            }
        ).sum();

    println!("total fuel needed: {}", total_fuel);
    Ok(())
}

fn calc_fuel_req(mass: u32) -> u32 {
    mass / 3 - 2
}

fn calc_fuel_req_rec(mass: u32) -> u32 {
    if mass < 6 {
        0
    } else {
        let fuel = mass / 3 - 2;
        fuel + calc_fuel_req_rec(fuel)
    }
}

fn calc_fuel_req_tail_rec(mass: u32, total: u32) -> u32 {
    if mass < 6 {
        total
    } else {
        let fuel = mass / 3 - 2;
        calc_fuel_req_tail_rec(fuel, total + fuel)
    }
}